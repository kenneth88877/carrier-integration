const integrationRouter = require('express').Router();
const fs = require('fs');
const path = require('path');

integrationRouter.get('/', async (req, res) => {
    console.log('Ni sud 1')

    const directoryPath = path.join('./csv');

    fs.readdir(directoryPath, (err, files) => {
        if (err) console.log(err);
        else {
        
            for (let i = 0; i < files.length ; i++) {
                let count = 0
                let dataArr = [];


                fs.readFile('./csv/'+files[i], "utf-8", (err, data) => {
                    
                    var rawData = data.split('\n'); 
                    rawData.forEach(row => {
                        var a = row;
                        var b = a.split('"||"'); 

                        let fName = b[0].split('"')
                
                        let amount = 0;
                        let date = '';
                        let costCenterNum = '';
        
                        if (b[6]) {
                            var cd = b[6].split('||'); 
                            if (cd[0]) costCenterNum = cd[0]
                            if (cd[1]) amount = cd[1]
                            if (cd[2]) { 
                                let z = cd[2].split('"')
                                date = z[1]
                            }
                        }
                        
                        let user = {
                            'name' : b[4],
                            'phone' : b[5],
                            'person' : {
                                'firstName' : fName[1],
                                'lastName' : b[1]
                            },
                            'amount' : amount,
                            'date' : date,
                            'costCenterNum' : costCenterNum
                        };
                
                        dataArr.push(user);

                    });

                    try {
                        if (count === files.length-1) res.status(200).send(dataArr.splice(1));
                    } catch (e) { }
                });
                count ++;
            };
        }
    });
});

module.exports = integrationRouter;