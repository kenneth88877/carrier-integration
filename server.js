const express = require('express')
let bodyParser = require('body-parser');

const app = express()
const port = process.env.PORT || 4000

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({ parameterLimit: 100000, limit: '50mb', extended: true }));
app.use(bodyParser.json({ parameterLimit: 100000, limit: '50mb', extended: true }));

const integrationRoutes = require('./routes/integrationRoutes');

app.use('/', integrationRoutes); 

app.listen(port, () => {
    console.log('app now listening for requests on port', port);
});
    